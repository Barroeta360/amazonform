

const amazonData = [
    //telefonos
    {
        "item": "telefono",
        "type": "electronics-intl-ship",
        "trademark": [
            {
                "mark": "Huawei",
                "url": ""//colocar url aqui
            },
            {
                "mark": "Samsung",
                "url": ""//colocar url aqui
            },
            {
                "mark": "Nokia",
                "url": ""//colocar url aqui
            },
            {
                "mark": "Alcatel",
                "url": ""//colocar url aqui
            },
            {
                "mark": "LG",
                "url": ""//colocar url aqui
            }

        ]
    }
    //camaras
    ,{
        "item": "camaras",
        "type": "electronics-intl-ship",
        "trademark": [
            {
                "mark": "Canon",
                "url": "",//colocar url aqui
                //"url": "https://www.amazon.com/s?k=camera&rh=p_89%3ACanon"//colocar url aqui
            },
            {
                "mark": "Sony",
                "url": ""//colocar url aqui
            },
            {
                "mark": "Nikon",
                "url": ""//colocar url aqui
            },
            {
                "mark": "Kodak",
                "url": ""//colocar url aqui
            },
            {
                "mark": "Fujifilm",
                "url": ""//colocar url aqui
            }

        ]
    }
]

const amazonURLStructure = [
    {
        "item": "camaras",
        "keyword": [
            {
                "pais": "USA",
                "k": "camera"
            },
            {
                "pais": "España",
                "k": "camara"
            }
        ],
        "codeServer":[
            {
                "trademark": "Canon",
                "code": "Canon"
            },
            {
                "trademark": "Sony",
                "code": "Sony"
            },
            {
                "trademark": "Nikon",
                "code": "Nikon"
            },
            {
                "trademark": "Kodak",
                "code": "Kodak"
            },
            {
                "trademark": "Fujifilm",
                "code": "Fujifilm"
            }
        ]
    },
    {
        "item": "telefono",
        "keyword": [
            {
                "pais": "USA",
                "k": "cell+phone"
            },
            {
                "pais": "España",
                "k": "telefono"
            }
        ],
        "codeServer":[
            {
                "trademark": "Huawei",
                "code": "Huawei"
            },
            {
                "trademark": "Samsung",
                "code": "Samsung"
            },
            {
                "trademark": "Nokia",
                "code": "Nokia"
            },
            {
                "trademark": "Alcatel",
                "code": "Alcatel"
            }
        ]
    }
]

const server = [
    {
        "pais": "USA",
        "url": "https://www.amazon.com/s?"
    },
    {
        "pais": "España",
        "url": "https://www.amazon.es/s?"
    }
]